<p>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
<br />
The GitLab logo and wordmark artwork are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
</p>
<p>
The word GitLab is a registered trademark.
Under fair use of US trademark law https://en.wikipedia.org/wiki/Fair_use_(U.S._trademark_law) a nonowner may use a trademark nominatively—to refer to the actual trademarked product or its source.
So if you want to use the word GitLab to refer the product or source that is distributed on https://about.gitlab.com/ that is OK, no need to ask us for permission.
</p>
